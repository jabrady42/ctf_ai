# The Strategy


### Strategy 1

Offense / Defense 

Agent 1 attempts to capture flag
Agent 2 defends flag


Agent 1 is determined by the top  position
Agent 2 is started in the bottom

Needs left right starting detect
May break if agents starts sandwiched between side and obstacle


### Agent 2

Move North to find flag

Navigate around flag planting mines
Move opposite start side 1
Move north 2
Move toward start side 1

Head north

Possible next steps
- Don't plant mine on last tile and wait for flag to move and chase flag

### Agent 1

Follow the wall to the flag and back
