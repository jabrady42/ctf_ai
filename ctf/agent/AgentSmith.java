package ctf.agent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import ctf.common.AgentAction;
import ctf.common.AgentEnvironment;

public class AgentSmith extends Agent {
	private boolean first;
	private int agentNum;
	private boolean startEast;
	private boolean startNorth;

	// Defender
	private boolean foundFlag;
	private boolean minesPlanted;
	private int mineStep;

	public AgentSmith() {
		System.out.println("Hello Mr Anderson..");
		this.first = true;
		this.agentNum = -1;
		this.mineStep = 0;
	}
	
	private void resetEverything() {
		this.resetD();
	}
	
	private void setupFirst(AgentEnvironment env) {
		this.first = false;
		System.out.println("First");
		// Determine agent number
		// 1 on top
		if(env.isObstacleNorthImmediate() && env.isBaseSouth(AgentEnvironment.OUR_TEAM, false)) {
			System.out.println("North Side");
			this.startNorth = true;
			this.agentNum = 1;
		} else if(env.isObstacleSouthImmediate() && env.isBaseNorth(AgentEnvironment.OUR_TEAM, false)) {
			System.out.println("South Side");
			this.startNorth = false;
			this.agentNum = 2;
		}
		
		if(env.isObstacleEastImmediate() && env.isBaseWest(AgentEnvironment.ENEMY_TEAM, false)) {
			this.startEast = true;
		} else if(env.isObstacleWestImmediate() && env.isBaseEast(AgentEnvironment.ENEMY_TEAM, false)){
			this.startEast = false;
		}
	}

	@Override
	public int getMove(AgentEnvironment env) {
		if(this.first) {
			this.setupFirst(env);
		} else if(this.isStartingPosition(env)) {
			System.out.println("Agent " + this.agentNum + " Starting position");
			this.resetEverything();
		}
		
 		int move = AgentAction.DO_NOTHING;
		
		if(this.isDefender() && !this.minesPlanted) {
			move = defendMove(env);
		} else {
			move = this.attackMove(env);
		}
		
		System.out.println("Agent " + this.agentNum + " move " + move);
		return move;
	}
	
	private boolean isStartingPosition(AgentEnvironment env) {
		boolean start = false;
		if(this.startNorth && this.startEast) {
			start = env.isBaseSouth(AgentEnvironment.OUR_TEAM, false) && 
					!env.isBaseEast(AgentEnvironment.OUR_TEAM, false) &&
					env.isObstacleNorthImmediate() &&
					env.isObstacleEastImmediate();
		}
		
		if(!this.startNorth && this.startEast) {
			start = env.isBaseNorth(AgentEnvironment.OUR_TEAM, false) && 
					!env.isBaseEast(AgentEnvironment.OUR_TEAM, false) &&
					env.isObstacleSouthImmediate() &&
					env.isObstacleEastImmediate();
		}
		
		if(this.startNorth && !this.startEast) {
			start = env.isBaseSouth(AgentEnvironment.OUR_TEAM, false) && 
					!env.isBaseWest(AgentEnvironment.OUR_TEAM, false) &&
					env.isObstacleNorthImmediate() &&
					env.isObstacleWestImmediate();
		}
		
		if(!this.startNorth && !this.startEast) {
			start = env.isBaseNorth(AgentEnvironment.OUR_TEAM, false) && 
					!env.isBaseWest(AgentEnvironment.OUR_TEAM, false) &&
					env.isObstacleSouthImmediate() &&
					env.isObstacleWestImmediate();
		}
		
		return start;
	}
	
	private boolean isDefender() {
		return this.agentNum == 1;
	}
	
	private int moveXOppositeStart() {
		if(this.startEast) {
			return AgentAction.MOVE_WEST;
		} else {
			return AgentAction.MOVE_EAST;
		}
	}
	
	private int moveXTowardStart() {
		if(this.startEast) {
			return AgentAction.MOVE_EAST;
		} else {
			return AgentAction.MOVE_WEST;
		}
	}
	
	private int moveYOppositeStart() {
		if(this.startNorth) {
			return AgentAction.MOVE_SOUTH;
		} else {
			return AgentAction.MOVE_NORTH;
		}
	}
	
	private int moveYTowardStart() {
		if(this.startNorth) {
			return AgentAction.MOVE_NORTH;
		} else {
			return AgentAction.MOVE_SOUTH;
		}
	}
	
	private int defendMove(AgentEnvironment env) {
		int move = AgentAction.DO_NOTHING;
		
		if(!this.foundFlag) {
		
			if(env.isBaseSouth(AgentEnvironment.OUR_TEAM, true)) {
				System.out.println("At the flag");
				this.foundFlag = true;
				move = AgentAction.PLANT_HYPERDEADLY_PROXIMITY_MINE;
			} else {
				// Move till flag it hit
				move = this.moveYOppositeStart();
			}
			
		} else if(!this.minesPlanted){
			move = navigateFlagMove(env);
		} else {
			move = this.moveYOppositeStart();
		}

		return move;
	}
	
	private void resetD() {
		this.minesPlanted = false;
		this.foundFlag = false;
		this.mineStep = 0;
	}
	
	private int navigateFlagMove(AgentEnvironment env) {
		List<Callable<Integer>> moveList = new ArrayList<Callable<Integer>>();
		
		moveList.add(() -> this.moveXOppositeStart());
		moveList.add(() -> AgentAction.PLANT_HYPERDEADLY_PROXIMITY_MINE);
		
		moveList.add(() -> this.moveYOppositeStart());
		moveList.add(() -> AgentAction.PLANT_HYPERDEADLY_PROXIMITY_MINE);
		
		moveList.add(() -> this.moveYOppositeStart());
		moveList.add(() -> AgentAction.PLANT_HYPERDEADLY_PROXIMITY_MINE);
		
		moveList.add(() -> this.moveXTowardStart());
		moveList.add(() -> AgentAction.PLANT_HYPERDEADLY_PROXIMITY_MINE);
		
		// Blow up
		moveList.add(() -> this.moveYOppositeStart());
		moveList.add(() -> AgentAction.PLANT_HYPERDEADLY_PROXIMITY_MINE);
		moveList.add(() -> this.moveXTowardStart());
		
		
		// Get move for this step
		int move = AgentAction.DO_NOTHING;
		try {
			move = moveList.get(this.mineStep++).call().intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(this.mineStep == 1) {
			System.out.println("Planting mines");
		}
		
		// When done
		if(this.mineStep >= moveList.size()) {
			this.minesPlanted = true;
			System.out.println("Planting mines complete");
//			this.resetD();
		}

		return move;
	}
	
	
	private int attackMove(AgentEnvironment env) {
		int move = AgentAction.DO_NOTHING;
		move = this.moveXOppositeStart();
		if(env.isObstacleEastImmediate() && env.isFlagNorth(AgentEnvironment.ENEMY_TEAM, false)) {
			move = AgentAction.MOVE_NORTH;
		} else if(env.hasFlag()) {
			move = AgentAction.MOVE_SOUTH;
			
			if(env.isObstacleSouthImmediate()) {
				move = this.moveXTowardStart();
			}
			
			if(env.isObstacleWestImmediate()) {
				move = AgentAction.MOVE_NORTH;
			}
			
		}
		return move;
	}

}
